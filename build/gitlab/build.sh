#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

_mvn() {
  if [ "${GITLAB_CI:-}" == "true" ]; then
    set -- \
      --batch-mode \
      -Dmaven.repo.local="${CI_PROJECT_DIR}/.m2/repository" \
      "$@"
  fi

  mvn "$@"
}

_mvn clean verify

if [ "${CI_COMMIT_BRANCH:-}" == "main" ]; then
  _mvn deploy -s ./build/gitlab/mvn-ci-settings.xml
fi
